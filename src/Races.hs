-- Copyright (C) 2019  Rukako
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, version 3 of the License.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

{-# LANGUAGE CPP #-}

#define MODULE Races
#include "header.hs"

import DoubleShow

data Race
  = MkRace
  { name :: Str
  , desc :: Str
  }

instance DoubleShow Race where
  show1 x = name x
  show2 x = desc x

yerles
  = MkRace "Yerles" "Also known as Humans"
fairy
  = MkRace "Fairy" ""
dwarf
  = MkRace "Dwarf" ""
elea
  = MkRace "Elea" "Also known as Elves"
snail
  = MkRace "Snail" ""
putit
  = MkRace "Putit" ""
lich
  = MkRace "Undead lich" ""
golem
  = MkRace "Golem" ""
childofgod
  = MkRace "Child of \"god\"" ""
mutant
  = MkRace "Mutant" ""
machine
  = MkRace "Machine" ""
nekomata
  = MkRace "Nekomata" ""
spirggan
  = MkRace "Spirggan" ""
tengu
  = MkRace "Tengu" ""
orge
  = MkRace "Orge" ""
vinestalker
  = MkRace "Vinestalker" ""
naga
  = MkRace "Naga" ""
formicid
  = MkRace "Formicid" ""
kobold
  = MkRace "Kobold" ""
octapode
  = MkRace "Octapode" ""
draconian
  = MkRace "Draconian" ""

races
  = [yerles
    ,fairy
    ,dwarf
    ,elea
    ,snail
    ,putit
    ,lich
    ,golem
    ,childofgod
    ,mutant
    ,machine
    ,nekomata
    ,spirggan
    ,tengu
    ,orge
    ,vinestalker
    ,naga
    ,formicid
    ,kobold
    ,octapode
    ,draconian]

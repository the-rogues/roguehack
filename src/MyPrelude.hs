-- Copyright (C) 2019  Rukako
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, version 3 of the License.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

{-# LANGUAGE CPP #-}

module MyPrelude where

#include "hide.hs"

import Numeric.Natural

clear = putStr "\ESC[2J\ESC[H"

type Nat = Natural
type Str = String

(!!) :: [a] -> Nat -> a
(!!) = genericIndex

length :: Foldable t => t a -> Nat
length lst = fromIntegral $ Prelude.length lst

drop :: Nat -> [a] -> [a]
drop = genericDrop

newline :: Char
newline = '\n'

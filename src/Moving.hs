-- Copyright (C) 2019  Rukako
-- Copyright (C) 2019  Rod Eric Joseph
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, version 3 of the License.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

{-# LANGUAGE CPP #-}

module Moving (Room (RoomC), Block (Empt, Wall, Floor), loop, name, width, blocks,
               entities, sym, mkBasicRoom) where

#include "header.hs"

import Entity

import System.IO hiding (print)

data Key = KUp
         | KDown
         | KRight
         | KLeft
         | KUpLeft
         | KUpRight
         | KDownLeft
         | KDownRight

data Block = Empt
           | Wall
           | Door
           | Floor
           | Comestible
           | Amulet
           | Armor
           | Money
           | Gem
           | Potion
           | Ring
           | Scroll
           | Spellbook
           | StairDown
           | StairUp
           | Tool
           | Boulder
           | Weapon
           | Wand

sym :: Block -> Char
sym Empt = ' '
sym Wall = '#'
sym Door = '+'
sym Floor = '.'
sym Comestible = '%'
sym Amulet = '\"'
sym Armor = '['
sym Money = '$'
sym Gem = '*'
sym Potion = '!'
sym Ring = '='
sym Scroll = '?'
sym Spellbook = '+'
sym StairDown = '>'
sym StairUp = '<'
sym Tool = '('
sym Boulder = '0'
sym Weapon = ')'
sym Wand = '/'

data Room = RoomC
  {
    name     :: String,
    width    :: Nat,
    blocks   :: [Block],
    entities :: [Entity]
  }

display e a pos = case lookup pos e of
                    Nothing -> sym a
                    Just a  -> a

instance Show Room where
  show (RoomC name width blocks entities) = print 0 ("Room: " ++ name ++ "\n")
                                            width blocks entities
    where print :: Nat -> String -> Nat -> [Block] -> [Entity] -> String
          print pos r w (a:as) e =
            print (pos + 1) (r ++ ((display e a pos):(case pos `mod` w == w - 1 of
                                                        True -> "\n"
                                                        _    -> ""))) w as e
          print _   r _ []     _  = r


-- Make a generic nxn room with wall around it
mkBasicRoom :: Nat -> Nat -> Room
mkBasicRoom xMax yMax = RoomC
  {
    name = "Room 1",
    width = xMax,
    blocks = [case (x, y) of
                (0, _) -> Wall
                (_, 0) -> Wall
                (x, y) | x == xMax - 1 || y == yMax - 1 -> Wall
                _ -> Floor
             | y <- [0..yMax-1], x <- [0..xMax-1]],
    entities = [(6, '@'),
                (7, sym Wall)]
  }
    
process :: Room -> IO Room
process room = do entities2 <- process2 (width room) (blocks room)
                               [] (entities room)
                  return $ room { entities = entities2 }

process2 :: Nat -> [Block] -> [Entity] -> [Entity] -> IO [Entity]
process2 width blocks r ((pos, '@'):xs) =
  do input <- getInput
     let pos2 = case input of
                  KUp        -> pos - width
                  KDown      -> pos + width
                  KLeft      -> pos - 1
                  KRight     -> pos + 1
                  KUpLeft    -> pos - width - 1
                  KUpRight   -> pos - width + 1
                  KDownLeft  -> pos + width - 1
                  KDownRight -> pos + width + 1 in
       process2 width blocks (r ++ [((case blocks !! pos2 of
                                        Wall -> pos
                                        _ -> case lookup pos2 (r ++ xs) of
                                                  Nothing -> pos2
                                                  _       -> pos2), '@')]) xs

process2 width blocks r (x:xs)          = process2 width blocks (r ++ [x]) xs
process2 _     _      r []              = return r


getInput :: IO Key
getInput = do hSetEcho stdin False
              hSetBuffering stdin NoBuffering
              c <- getChar
              case c of
                '8' -> return KUp
                'k' -> return KUp
                '2' -> return KDown
                'j' -> return KDown
                '4' -> return KLeft
                'h' -> return KLeft
                '6' -> return KRight
                'l' -> return KRight
                '7' -> return KUpLeft
                'y' -> return KUpLeft
                '9' -> return KUpRight
                'u' -> return KUpRight
                '1' -> return KDownLeft
                'b' -> return KDownLeft
                '3' -> return KDownRight
                'n' -> return KDownRight
                '\ESC' -> do c <- getChar
                             case c of
                               '[' -> do c <- getChar
                                         case c of
                                           'D' -> return KLeft
                                           'C' -> return KRight
                                           'A' -> return KUp
                                           'B' -> return KDown
                _   -> do putStrLn "Try again!"
                          getInput

print :: Room -> IO ()
print room = do clear
                putStr $ show room


loop :: Room -> IO ()
loop room  = do print room
                process room >>= loop

-- geti w (x, y) = y * w + x

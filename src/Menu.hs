-- Copyright (C) 2019  Rukako
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, version 3 of the License.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

{-# LANGUAGE CPP #-}

#define MODULE Menu
#include "header.hs"

import System.IO
import DoubleShow

-- description handling, sl working properly, up down etc keys

keys = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p']

-- Displays and handles the input of the menu
menu :: (DoubleShow x, DoubleShow y)
     => x    -- Title[Str]
     -> Bool -- Can exit?
     -> [y]  -- Values to display
     -> IO (Maybe Nat)
menu title esc lst
  = menu' title esc keys 0 0 lst

menu' :: (DoubleShow x, DoubleShow y)
      => x    -- Title[Str]
      -> Bool -- Can exit?
      -> [Char] -- Keys
      -> Nat  -- Page
      -> Nat  -- Selected line
      -> [y]  -- Values to display
      -> IO (Maybe Nat)
menu' title esc k p sl lst
  = do hSetEcho stdin False
       hSetBuffering stdin NoBuffering
       clear
       res <- menu'' (show1 title ++ [newline]) esc k (length lst `div` (length k + 1)) p sl lst
       return res

menu'' :: DoubleShow x
       => Str    -- Title
       -> Bool   -- Can exit?
       -> [Char] -- Keys
       -> Nat    -- Max pages
       -> Nat    -- Page
       -> Nat    -- Selected line
       -> [x]    -- Values to display
       -> IO (Maybe Nat)
menu'' title esc k mp p sl lst
  = do putStr $ loopmenu title esc k mp p sl 0 (drop (length k * p) lst)
       c <- getChar
       clear
       case c of
         '\n'   -> return $ Just $ length k * p + sl
         '4'    -> left p
         '6'    -> right p
         '8'    -> up p sl
         '2'    -> down p sl
         '\ESC' -> do c <- getChar
                      case c of
                        '[' -> do c <- getChar
                                  case c of
                                    'D' -> left p
                                    'C' -> right p
                                    'A' -> up p sl
                                    'B' -> down p sl
                                    _   -> err "Unknown key sequence"
                        _    -> err "Unknown key sequence"

         'c'
           | esc  -> return Nothing
           | True -> err "Can't escape"
         c        -> err $ "Unknown key: " ++ show c
  where
    left  0 = menu'' title esc k mp mp      0 lst
    left  p = menu'' title esc k mp (p - 1) 0 lst
    right p
      | p == mp = menu'' title esc k mp 0       0 lst
      | True    = menu'' title esc k mp (p + 1) 0 lst
    up    p 0
      | p == mp = menu'' title esc k mp p (length lst `mod` length k - 1) lst
      | True    = menu'' title esc k mp p (length k - 1)                  lst
    up    p l   = menu'' title esc k mp p (l - 1)                         lst
    down  p l
      | l == length k - 1
        || (p == mp && l >= length lst `mod` length k - 1) = menu'' title esc k mp p 0 lst
      | True = menu'' title esc k mp p (sl + 1) lst
    err s = do putStrLn $ ('!':'!':s) ++ "!!"
               menu'' title esc k mp p sl lst

loopmenu :: DoubleShow x
         => Str    -- Return
         -> Bool   -- Can exit?
         -> [Char] -- Keys
         -> Nat    -- Max page
         -> Nat    -- Page
         -> Nat    -- Selected line
         -> Nat    -- Current line
         -> [x]    -- Values to display
         -> Str

loopmenu r e _ mp p _ _  [] = showfinal r e mp p
loopmenu r e k mp p _ cl _
  | cl == length k = showfinal r e mp p

loopmenu r esc k mp p sl cl (x:xs)
  = loopmenu (r ++ ((case cl == sl of
                       True -> '*'
                       _    -> ' '):' ':(k !! cl):'.':' ':(show1 x ++ [newline]))) esc k mp p sl (cl + 1) xs

showfinal :: Str  -- Return
          -> Bool -- Can exit?
          -> Nat  -- Max page
          -> Nat  -- Page
          -> Str
showfinal r esc mp p
  = r
    ++ case mp of
         0 -> ""
         _ -> "4, <- - Previous page" ++ [newline]
              ++ "6, -> - Next page"  ++ [newline]
    ++ "8, ^  - Up"    ++ [newline]
    ++ "2, \\/ - Down" ++ [newline]
    ++ case esc of
         True -> "c     - Exit"  ++ [newline]
         _    -> ""
    ++ case mp of
         0 -> ""
         _ -> show (p + 1)
              ++ '/':show (mp + 1)
              ++ [newline]
